/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.senac.exemplo10.test;

import br.com.senac.exemplo10.Aluno;
import br.com.senac.exemplo10.AlunoNota;
import java.util.ArrayList;
import java.util.List;
import org.junit.Test;
import static org.junit.Assert.*;

public class AlunoTest {

    public AlunoTest() {
    }

    @Test
    public void QuantosAcimaDaNota7() {

        Aluno aluno1 = new Aluno("Jamie", 8, 8);
        Aluno aluno2 = new Aluno("Rincon", 7, 8);
        Aluno aluno3 = new Aluno("Jorge", 6, 5);

        List<Aluno> lista = new ArrayList<>();
        lista.add(aluno1);
        lista.add(aluno2);
        lista.add(aluno3);
        AlunoNota nota = new AlunoNota();
        double resultado = nota.AlunosNota(lista);

        assertEquals(2,resultado, 0.01);

    }
}