/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.senac.exemplo10;

import java.util.List;

public class AlunoNota {

    public double AlunosNota(List<Aluno> lista) {
        double contar = 0;

        for (Aluno aluno : lista) {
            int nota = (int) ((aluno.getNota1() + aluno.getNota2()) / 2);
            if (nota >= 7) {
                contar = contar + 1;
            }
        }
        return contar;
    }

}
